<?php

namespace Swissclinic\AmastyGeoRedirect\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper {

    const XML_PATH_SWISSCLINIC_AMASTYGEOREDIRECT_CONFIG= 'swissclinic_amasty_geo_redirect/swissclinic_amasty_geo_redirect_config/enable';

    public function __construct(
        \Magento\Framework\App\Helper\Context $context
    ) {
        parent::__construct($context);
    }


    /**
     * @param null $storeId
     * @return mixed
     */
    public function isEnabled($storeId = null)
    {
        $isEnabled = $this->scopeConfig->getValue(
            self::XML_PATH_SWISSCLINIC_AMASTYGEOREDIRECT_CONFIG,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );

        return $isEnabled;
    }
}