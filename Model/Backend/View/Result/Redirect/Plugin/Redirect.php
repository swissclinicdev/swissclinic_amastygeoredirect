<?php

namespace Swissclinic\AmastyGeoRedirect\Model\Backend\View\Result\Redirect\Plugin;

use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Framework\Stdlib\Cookie\PublicCookieMetadata;
use Magento\Store\Model\StoreManagerInterface;
use Swissclinic\AmastyGeoRedirect\Helper\Data;


class Redirect {

    private $cookieInterface;

    private $publicCookieMetadata;

    private $storeManager;

    private $helper;

    public function __construct(
        CookieManagerInterface $cookieInterface,
        PublicCookieMetadata $publicCookieMetadata,
        StoreManagerInterface $storeManager,
        Data $helper
    )
    {
        $this->cookieInterface = $cookieInterface;
        $this->publicCookieMetadata = $publicCookieMetadata;
        $this->storeManager = $storeManager;
        $this->helper = $helper;
    }

    public function beforeSetUrl(\Magento\Backend\Model\View\Result\Redirect $subject, $url) {

        $store = $this->storeManager->getStore()->getId();

        $isEnabled = $this->helper->isEnabled($store);

        if($isEnabled) {
            $pos = strpos($url, '?___from_store=');

            if($pos !== false) {
                $cookie = $this->cookieInterface->getCookie('geo_ip_redirect');
                if (!$cookie) {
                    $this->publicCookieMetadata->setDurationOneYear();
                    $this->cookieInterface->setPublicCookie('geo_ip_redirect', '1', $this->publicCookieMetadata);
                }

            }
        }

        return $url;
    }
}